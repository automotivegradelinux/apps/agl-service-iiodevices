/*
 * Copyright (C) 2019 "IoT.bzh"
 * Author "Clément Bénier" <clement.benier@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _CONFIG_IIODEVICES_
#define _CONFIG_IIODEVICES_

#include <stdio.h>
#include <stdlib.h>

#define IIODEVICE "/sys/bus/iio/devices/"

struct iio_info {
    const char *dev_name;
    const char *id;
    const char *middlename;
};

enum iio_elements { X = 1, Y = 2, Z = 4 };

void set_channel_name(char *name, enum iio_elements iioelts);
enum iio_elements treat_iio_elts(const char *iioelts_string);
int get_iio_nb(enum iio_elements iioelts);

#endif //_CONFIG_IIODEVICES_
