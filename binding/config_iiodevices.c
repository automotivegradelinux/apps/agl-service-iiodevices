/*
 * Copyright (C) 2019 "IoT.bzh"
 * Author "Clément Bénier" <clement.benier@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "config_iiodevices.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AFB_BINDING_VERSION 3
#include <afb/afb-binding.h>

void set_channel_name(char *name, enum iio_elements iioelts)
{
    switch(iioelts) {
        case X:
            strcat(name, "_x"); break;
        case Y:
            strcat(name, "_y"); break;
        case Z:
            strcat(name, "_z"); break;
        default:
            AFB_API_WARNING(afbBindingV3root, "cannot set channel name, no matching iio_elements found: %d", iioelts);
    }
}

enum iio_elements treat_iio_elts(const char *iioelts_string)
{
    enum iio_elements iioelts = 0;
    if(!iioelts_string || !strcasecmp(iioelts_string, ""))
        iioelts = X | Y | Z;
    else {
        if(strchr(iioelts_string, 'x') || strchr(iioelts_string, 'X'))
            iioelts = iioelts | X;
        if(strchr(iioelts_string, 'y') || strchr(iioelts_string, 'Y'))
            iioelts = iioelts | Y;
        if(strchr(iioelts_string, 'z') || strchr(iioelts_string, 'Z'))
            iioelts = iioelts | Z;
    }
    AFB_API_INFO(afbBindingV3root, "ENUMERATION::: %s %d", iioelts_string, iioelts);
    return iioelts;
}

int get_iio_nb(enum iio_elements iioelts)
{
    int nb_iioelts = 0;
    int tmp_mask = 1;
    while(tmp_mask <= iioelts) { //the number of channels is the number of elements
        if((iioelts & tmp_mask) == tmp_mask)
            nb_iioelts++;
        tmp_mask = tmp_mask << 1;
    }
    return nb_iioelts;
}
